`use strict`;

const
    fs        = require(`fs`),
    Sequelize = require(`sequelize`),
    Op        = Sequelize.Op;

module.exports = (path) => {

    global.db    = {};
    global.Op    = Op;

    return new Promise((resolve, reject) => {

        if(!environment.db.host) {
            return resolve();
        }

        const sequelize = new Sequelize(environment.db.name, environment.db.user, environment.db.pass, {
            host    : environment.db.host,
            port    : environment.db.port,
            dialect : environment.db.engine,
            schema  : environment.db.schema,
            logging : environment.db.debug ? console.log : false,
            dialectOptions: {
              useUTC: false, //for reading from database
              dateStrings: true,
              typeCast: function (field, next) { // for reading from database
                if (field.type === 'DATETIME') {
                  return field.string()
                }
                  return next()
                },
            },
            timezone: '+07:00'

        });

        sequelize.authenticate().then(() => {
            if(!path) {
                path = `${basedir}/src/schemas`;
            }
            
            // load schema
            fs.readdirSync(path).forEach((file, idx) => {

                if(file.replace(/.js|.ts/g, ``) === `relation`) {
                    return;
                }

                let model = require(`${path}/${file}`);

                global.db[file.replace(/.js|.ts/g, ``)] = model(sequelize);
            });

            global.sequelize = sequelize;

            try {
                require(`${basedir}/src/schemas/relation`)();
            }
            catch(error) {
                // 
            }

            resolve(sequelize);
        }).catch((error) => {
            reject(error);
        });
    });

};
