/**
 * @file        : mqtt.js
 * @author      : PT. Elnusa, Tbk.
 * @description : MQTT Connector
 */

`use strict`;

const mqtt = require(`mqtt`);

const
    host = environment.mqtt.host,
    port = environment.mqtt.port;

let client,
    appId   = environment.app.name;

const connect = (topic) => {
    return new Promise((resolve, reject) => {

        const url = `${host}:${port}`;

        const Client = mqtt.connect(url, {
            clientId : Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15), //generate random string
            clean    : false
        });

        Client.on(`connect`, () => {
            if(topic.constructor === Array || topic.constructor === Object) {
                for(let i in topic) {
                    Client.subscribe(`${topic[i]}/#`, { retain: false, qos: 1 });
                }
            }
            else {
                Client.subscribe(`${topic}/#`, { retain: false, qos: 1 });
            }

            resolve(Client);
        });
    }).catch(() => {
        throw new Error(`Cannot connect to MQTT Broker.`);
    });
};

const main = {
    publish : (target, topic, payload, options) => {
        if(payload.constructor === String) {
            payload = JSON.parse(payload);
        }

        options = options ? options : { retain: false, qos: 0 };

        const data = {
            headers   : payload.headers,
            auth      : payload.auth,
            params    : payload.params ? payload.params : payload,
            body      : payload.body,
            queue     : payload.queue,
            requestid : payload.requestid,
            return    : payload.return,
            source    : payload.source ? payload.source : appId,
            topic     : topic,
        };

        client.publish(`${target}`, JSON.stringify(data), options);
    },

    on : (type, callback) => {
        if(type === `message`) {
            main.onMessage(callback);
            return;
        }

        client.on(type, callback);
    },

    onMessage : (callback) => {
        client.on(`message`, (topic, payload) => {
            const data = JSON.parse(payload);
            
            callback(topic, data);
        });
    }
};

module.exports = async (topic = appId) => {
    return new Promise(async (resolve, reject) => {
        client = await connect(topic);

        global.mqtt = main;
    
        resolve({ ... client });
    })
    
};
