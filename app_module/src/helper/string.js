/**
 * @file        : string.js
 * @description : String helper
 */

`use strict`;

module.exports = {

    /**
     * Encode html tag into coded character
     * @param  {String} html => Source html
     * @return {String}
     */
    htmlescape : (html) => {
        return String(html)
        .replace(/&(?!\w+;)/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    },

    /**
     * Generate n of random string
     * @param  {Integer} num => char length
     * @return {String}
     */
    random : (num) => {
        if(!num) {
            return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        }

        const set = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let text  = "";

        for (let i=0; i<num; i++) {
            text += set.charAt(Math.floor(Math.random() * set.length));
        }

        return text;
    },

    /**
     * Auto fill character into string to match n length
     * @param  {String}  text    => Source text
     * @param  {String}  padding => Padding character
     * @param  {Integer} length  => String length
     * @return {String}
     * 
     * @example : fill(123, '0', 5) => '00123'
     */
    fill : (text, padding, length) => {
        const str = (text.constructor !== String ) ? text.toString() : text;

        return str.length < length ? string.fill(padding + str, padding, length) : str;
    },
};