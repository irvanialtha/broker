/**
 * @file        : time.js
 * @description : Time/Date helper
 */

`use strict`;

module.exports = {

    /**
     * Get today's date in dd/mm/yyyy
     * @param  {Boolean} time   => Display time (hh:mm)
     * @return {String}
     */
    today: (time = false) => {
        let today  = new Date(),
            dd     = today.getDate(),
            mm     = today.getMonth() + 1, //January is 0!
            yyyy   = today.getFullYear(),
            hour   = today.getHours(),
            minute = today.getMinutes(),
            second = today.getSeconds();

        if(dd < 10){
            dd = '0' + dd;
        }

        if(mm < 10) {
            mm = '0' + mm;
        }

        let date = dd + '/' + mm + '/' + yyyy;

        if(time) {
            date = `${date} ${hour < 10 ? `0${hour}` : hour}:${minute < 10 ? `0${minute}` : minute}:${second < 10 ? `0${second}` : second}`;
        }

        return date;
    },

    /**
     * Get month
     * @param  {String/Integer} index => Month's index in range of 1-12
     * @param  {String}         lang  => Language (en/id)
     * @param  {Boolean}        short => Slice month (January => Jan, February => Feb)
     * @return {String}
     */
    getMonth : (index, lang = `en`, short = false) => {
        if(!index) {
            throw new Error(`Undefined month's index.`);
        }

        try {
            index = parseInt(index);
        }
        catch(error) {
            throw new Error(`Invalid index's format.`);
        }
        
        let month = {
            en : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            id : ['Januari', 'Febuari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        };

        try {
            switch(lang) {
                case `id`:
                    month = month.id[index-1];
                break;
                default:
                    month = month.en[index-1];
                break;
            }

            if(short) {
                month = month.slice(0, 3);
            }
        }
        catch(error) {
            throw new Error(error);
        }

        return month
    }
};