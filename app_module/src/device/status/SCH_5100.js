/**
 * @file        : status.js
 * @author      : Grendy Eki A
 * @description : Device Parameter Details
 */

'use strict';

const
	deviceActive	= ['Inactive', 'Active'],
	batteryLevel 	= ['No Power', 'Extremely Low', 'Very Low', 'Low', 'Medium', 'High', 'Very High','Undefined'],
	signalLevel 	= ['No Signal', 'Extremely Weak', 'Very Weak', 'medium', 'Strong'],
	deviceStatus 	= [
		'Normal', 						//0
		'SOS', 							//1
		'Enter Geofence', 				//2
		'Exit Geofence', 				//3
		'Power ON', 					//4
		'Power OFF', 					//5
		'Periodic Mode', 				//6
		'Motion Mode: Static',			//7
		'Motion Mode: Moving', 			//8
		'Motion Mode: Static to Moving',//9
		'Motion Mode: Moving to Static',//10
		'Help',							//11
		'Low Battery',					//12
		'Power on (temperature)',		//13
		'Power off (low battery)',		//14
		'Power off (temperature)'		//15
	],
	alertLevel 		= ['normal', 'low', 'moderate', 'critical'],
	alertColor 		= { low: 'pv-alert-low', moderate: 'pv-alert-moderate', critical: 'pv-alert-critical'},
	minute_in_seconds = 60 * 1000,
    hour_in_seconds = minute_in_seconds * 60,
    day_in_seconds 	= hour_in_seconds * 24,
    inactive_device_time_limit = hour_in_seconds * .5,
    active_coordinate_treshold = 0.000001;

module.exports = {
	deviceStatusDesc(val) {
		return typeof deviceStatus[val] !== 'undefined' ? deviceStatus[val] : '';
	},
	batteryLevelDesc(val) {
		return typeof batteryLevel[val] !== 'undefined' ? batteryLevel[val] : '';
	},
	signalLevelDesc(val) {
		return typeof signalLevel[val] !== 'undefined' ? signalLevel[val] : '';
	},
	isActiveDevice(deviceTime) {
		if (Date.now() - deviceTime < inactive_device_time_limit) {
			return 1;
		} else {
			return 0;
		}
	},
	activeDeviceDesc(val) {
		return typeof deviceActive[val] !== 'undefined' ? deviceActive[val] : '';
	}
};
