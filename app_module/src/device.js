'use strict';

const 
    fs  = require(`fs`),
    obj = {};

fs.readdirSync(`${__dirname}/device/parser`).forEach((file) => {
    const src = require(`${__dirname}/device/parser/${file}`);

    obj[file.replace(/.js|.ts/g, ``)] = src;
});

 module.exports = { ... obj };