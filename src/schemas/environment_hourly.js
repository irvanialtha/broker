`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`environment_hourly`, {
        hourly        : {
            type      : Sequelize.DATE,
            allowNull : false,
            primaryKey: true
        },
        client_id : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        device_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        humidity : {
            type      : Sequelize.FLOAT,
            allowNull : false
        },
        temperature : {
            type      : Sequelize.FLOAT,
            allowNull : false
        },
        freq : {
            type      : Sequelize.FLOAT,
            allowNull : false
        },
        voltage : {
            type      : Sequelize.FLOAT,
            allowNull : false
        }
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'public'
    });

};