`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`metrics`, {
        time        : {
            type      : Sequelize.DATE,
            allowNull : false,
            primaryKey: true
        },
        client_id : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        device_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        data  : {
            type      : Sequelize.JSONB,
            allowNull : false
        }
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'public'
    });

};