`use strict`;
const
    { redis }       = require(`${basedir}/app_module/src/helper`);
module.exports = {
    device: async (data) =>{
        //** Delete redis for old stream data */
        redis.delete(`*${data.params.socketid}*`);

        //** Create redis for new stream data */
        let
            key = `${data.params.deviceName}::${data.params.socketid}`
            redis.set(key,data, 2);
        
        //** Get Last Data form Database */
        let lastData = await model.main.getdatadevice(data.params);
        for (let i in lastData) {
            let output = {
                params : {
                    deviceName  : `${data.params.deviceName}`,
                    socketid    : data.params.socketid,
                    total       : lastData[i].total || null,
                    ...lastData[i].data
                }
            }
            amqp.publish(service.socket,'realtime.device',output);
        }
    },
    devicesummary: async (data) =>{
        //** Delete redis for old stream data */
        redis.delete(`*${data.params.socketid}*`);

        //** Create redis for new stream data */
        let
            key = `${data.params.deviceName}::${data.params.socketid}`
            redis.set(key,data, 2);
        
        //** Get Last Data form Database */
        let lastData = await model.main.getlastdatadaily(data.params.deviceName);
        for (let i in lastData) {
            let output = {
                params : {
                    deviceName  : `${data.params.deviceName}`,
                    socketid    : data.params.socketid,
                    ...lastData[i]
                }
            }
            amqp.publish(service.socket,'realtime.devicesummary',output);
        }
    },
    closedevice: async (data) =>{
        //** Delete redis for old stream data */
        redis.delete(`*${data.params.socketid}*`);
    }
}