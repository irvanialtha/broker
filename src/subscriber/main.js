`use strict`;
const
    { time }       = require(`${basedir}/app_module/src/helper`);
module.exports = {
    data: (data) =>{
        //** Stream data to Socket */
        control.realtime.device(data);
        control.main.getdetailclient(data)   
    },
    savedata: (data) =>{
        let params = data.params,
            client = params.client,
            device = params.device;
    
        let insertdata ={
            time        : device.properties.time,
            client_id   : client.client_code,
            device_name : device.properties.devicename,
            data        : device 
        }
        
        db.metrics.create(insertdata).then((values) =>{
            console.log(`${time.today(true)} : ${device.properties.devicename} Insert`);
        }).catch((error) =>{
            console.log(error)
        })
    }
}