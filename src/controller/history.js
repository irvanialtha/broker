`use strict`;

module.exports = {
    
    appliance : async (data) => {
        let params      = data.body,
            deviceids   = params.deviceids,
            daterange   = params.daterange,
            price       = params.price || null,
            output      = {};
        switch (daterange) {
            case 'hourly':
                output = await model.history.getenergyusagehourly(deviceids);
                break;
            case 'daily':
                output = await model.history.getenergyusagedaily(deviceids);
                break;
            case 'monthly':
                output = await model.history.getenergyusagemonthly(deviceids);
                break;
            case 'yearly':
                output = await model.history.getenergyusageyearly(deviceids);
            break;
        
            default:

                break;
        }

        if (price) {
            output.map((item) =>{
                item.currency = price.currency
                if (price.currency == 'IDR') {
                    item.cost = parseFloat(item.value * price.price).toFixed(0)
                }else {
                    item.cost = parseFloat(item.value * price.price).toFixed(2)
                }
            })
        }
        data.body = {
            success : true,
            data :output
        };
        amqp.publish(data.source,'client.output',data);
    },
    usageperdevice: async (data) => {
        let params      = data.body,
            deviceids   = params.deviceids,
            daterange   = params.daterange,
            price       = params.price || null,
            output      = {};
        switch (daterange) {
            case 'hourly':
                output = await model.history.getdeviceenergyusagehourly(deviceids);
                break;
            case 'daily':
                output = await model.history.getdeviceenergyusagedaily(deviceids);
                break;
            case 'monthly':
                output = await model.history.getdeviceenergyusagemonthly(deviceids);
                break;
            case 'yearly':
                output = await model.history.getdeviceenergyusageyearly(deviceids);
            break;
        
            default:

                break;
        }

        if (price) {
            output.map((item) =>{
                item.currency = price.currency
                if (price.currency == 'IDR') {
                    item.cost = parseFloat(item.value * price.price).toFixed(0)
                }else {
                    item.cost = parseFloat(item.value * price.price).toFixed(2)
                }
            })
        }
        
        data.body = {
            success : true,
            data :output
        };
        amqp.publish(data.source,'client.output',data);
    },
    usageperfloor: async (data) => {
        let params      = data.body,
            floor       = params.floor,
            daterange   = params.daterange,
            price       = params.price || null,
            output      = [];
        for (let i in floor) {
            let floorusage = {},
                totalusage = 0;
            switch (daterange) {
                case 'hourly':
                    floorusage = await model.history.getdeviceenergyusagehourly(floor[i].device);
                    break;
                case 'daily':
                    floorusage = await model.history.getdeviceenergyusagedaily(floor[i].device);
                    break;
                case 'monthly':
                    floorusage = await model.history.getdeviceenergyusagemonthly(floor[i].device);
                    break;
                case 'yearly':
                    floorusage = await model.history.getdeviceenergyusageyearly(floor[i].device);
                break;
            
                default:
    
                    break;
            }
            for (let j in floorusage) {
                totalusage += parseFloat(floorusage[j].value)
                
            }
            output.push({
                floor : floor[i].floor,
                value : parseFloat(totalusage).toFixed(2)
            })
            output.sort((a, b) => (a.value > b.value) ? -1 : 1)
        }

        if (price) {
            output.map((item) =>{
                item.currency = price.currency
                if (price.currency == 'IDR') {
                    item.cost = parseFloat(item.value * price.price).toFixed(0)
                }else {
                    item.cost = parseFloat(item.value * price.price).toFixed(2)
                }
            })
        }

        data.body = {
            success : true,
            data :output
        };
        amqp.publish(data.source,'client.output',data);  
    },
    exportdevice: async (data) => {
        //** Get Last Data form Database */
        let output  = []
            history = await model.main.getdatadevice(data.params);

        for (const i in history) {
            let historyData = history[i].data,
                properties  = historyData.properties,
                val         = {}
                for (let i in properties) {
                    if (typeof properties[i] == 'number') {
                        val[`${i}`]  = properties[i]
                    }
                }
            output.push(val)
        }
        data.body = {
            success : true,
            data :output
        };
        amqp.publish(data.source,'client.exportdevice',data);  
    },
    exportdevice2: async (data) => {
        let history = await model.main.getdatadevice2(data.params);
        data.body = {
            success : true,
            data : history
        };
        amqp.publish(data.source,'client.exportdevice',data);  
    }
};