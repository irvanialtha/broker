`use strict`;
const
    { time, redis }  = require(`${basedir}/app_module/src/helper`);
module.exports = {
    
    device : (data) => {
        let params      = data.params,
            deviceName  = params.properties.devicename;
        //** Get list all socket active for device */
        cache.scan(deviceName, async (err, result) => {
            //** Get Socket Id's */
            for (const i in result) {
                //** Send data to service Socket*/
                let 
                    socketData = await redis.get(result[i])
                    
                    data.params.socketid    = socketData.params.socketid;
                    data.params.devicename  = deviceName;
                    amqp.publish(service.socket,'realtime.device',data);
            }
            
        })
    }
};