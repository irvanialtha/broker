`use strict`;
const { QueryTypes, DATE } = require("sequelize");
module.exports = {
    data:()=>{

    },

    getlastdata: (params)=>{
        let output = []
        return new Promise((resolve,reject) =>{
            db.metrics.findAll({
                order: [
                    ['time', 'DESC']
                ],
                where: {
                    device_name : {
                        [Op.eq]: params.deviceName
                    }
                },
                limit : 10
            }).then((rows) =>{
                for (const i in rows) {
                    output.push(rows[i].dataValues.data)
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
    
    getdatadevice: (params)=>{
        let output = [],
            query  = '';
            
        if (!isNaN(parseInt(params.daterange)) ) {
            query = `AND time > current_timestamp - interval '${params.daterange} minutes'`;
        } else {
            switch (params.daterange) {
                case 'today':
                query = `AND time >= current_date::timestamp and time < current_date::timestamp + interval '1 day'`;
                    break;
                case 'date':
                    query = `AND to_char(time,'DD.MM.YYYY') = '${params.date}'`;
                        break;
                default:
                    query = `AND time >= current_date::timestamp and time < current_date::timestamp + interval '1 day'`;
                    break;
            }
        }
        return new Promise((resolve,reject) =>{
            sequelize.query(`
            SELECT * , (
                SELECT count(*) 
                FROM metrics
                WHERE device_name = '${params.deviceName}'
                ${query}
            ) as count
            from metrics m2 
            WHERE 
                device_name = '${params.deviceName}'
                ${query}
            ORDER BY m2.time ASC
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.metrics
            })
            .then((result)=>{
                for (let i in result) {
                    output.push({
                            total : result[i].count,
                            data : result[i].data
                    })
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },

    getdatadevice2: (params)=>{
        let output = [],
            field  = ``,
            device_name = ``,
            time = ``;

        for (let i in params.field) {
            if (i == 0) {
                field += `${params.field[i]}`;
            } else {
                field += `,${params.field[i]}`;
            }
        }

        for (let i in params.device_name) {
            if (i == 0) {
                device_name += `'${params.device_name[i]}'`;
            } else {
                device_name += `,'${params.device_name[i]}'`;
            }
        }

        for (let i in params.time) {
            if (i == 0) {
                time += `'${params.time[i]}'`;
            } else {
                time += `,'${params.time[i]}'`;
            }
        }

        return new Promise((resolve,reject) =>{
            sequelize.query(`
                select 
                    hourly as time,
                    client_id,
                    device_name,
                    ${field}
                from environment_hourly
                where
                    client_id  = '${params.client_id}' and
                    device_name in (${device_name}) and
                    to_char(hourly, 'hh24:mi') in (${time}) and
                    hourly between '${params.start_date}' and '${params.end_date}'
                order by time asc
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.environment_hourly
            })
            .then((result)=>{
                resolve(result)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },


    getlastdatadaily: (deviceName)=>{
        let output = []
        
        return new Promise((resolve,reject) =>{
            sequelize.query(`
                SELECT time_bucket('1 day', time) AS daily, last(cast(data->'properties'->'activeEnergy' as float)/100, time) as power 
                FROM 
                    metrics WHERE device_name = '${deviceName}'
                AND time > NOW() - INTERVAL '10 days' GROUP BY daily ORDER BY daily ASC LIMIT 10;
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.metrics
            })
            .then((result)=>{
                for (let i in result) {
                    let date = new Date,
                        daily = `${result[i].daily}`,
                        timestamp = new Date(daily).getTime();

                    output.push({
                        properties : {
                            time : timestamp,
                            power : result[i].power
                        }
                    })
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
}