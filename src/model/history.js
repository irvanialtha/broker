`use strict`;
const { QueryTypes, DATE } = require("sequelize");
module.exports = {
    getenergyusagehourly: (deviceids)=>{
        let output = [],
            query  = ``;

        for (let i in deviceids) {
            if (i == 0) {
                query += `'${deviceids[i]}'`
            } else{
                query += `,'${deviceids[i]}'`

            }
        }
        
        return new Promise((resolve,reject) =>{
            sequelize.query(`
            SELECT * FROM(
                SELECT 
                    hour AS hourly, avg("usage") AS energy 
                FROM 
                    energy_usage_hourly eud
                WHERE 
                    device_name in (${query})
                AND 	
		            hour >= date_trunc('day', now())
                GROUP BY 
                    hour
                ORDER BY 
                    hour desc 
            ) A
            ORDER BY A.hourly ASC
            ;
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.metrics
            })
            .then((result)=>{
                for (let i in result) {
                    let date = new Date,
                        hourly = `${result[i].hourly}`,
                        timestamp = new Date(hourly).getTime();

                    output.push({
                            time : timestamp,
                            value : parseFloat(result[i].energy).toFixed(2)
                    })
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
    getenergyusagedaily: (deviceids)=>{
        let output = [],
            query  = ``;

        for (let i in deviceids) {
            if (i == 0) {
                query += `'${deviceids[i]}'`
            } else{
                query += `,'${deviceids[i]}'`

            }
        }
        
        return new Promise((resolve,reject) =>{
            sequelize.query(`
            SELECT * FROM(
                SELECT 
                    "day" AS daily, avg("usage") AS energy 
                FROM 
                    energy_usage_daily eud
                WHERE 
                    device_name in (${query})
                GROUP BY 
                    "day"
                ORDER BY 
                    "day" desc 
                LIMIT
                    30
            ) A
            ORDER BY A.daily ASC;
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.metrics
            })
            .then((result)=>{
                for (let i in result) {
                    let date = new Date,
                        daily = `${result[i].daily}`,
                        timestamp = new Date(daily).getTime();

                    output.push({
                            time : timestamp,
                            value : parseFloat(result[i].energy).toFixed(2)
                    })
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
    getenergyusagemonthly: (deviceids)=>{
        let output = [],
            query  = ``;

        for (let i in deviceids) {
            if (i == 0) {
                query += `'${deviceids[i]}'`
            } else{
                query += `,'${deviceids[i]}'`

            }
        }
        
        return new Promise((resolve,reject) =>{
            sequelize.query(`
            SELECT * FROM (
                SELECT 
                    monthly, avg(total_usage) AS energy 
                FROM 
                    energy_usage_monthly eud
                WHERE 
                    device_name in (${query})
                GROUP BY 
                    monthly
                ORDER BY 
                    monthly desc 
                LIMIT
                    5
            ) A
            ORDER BY A.monthly ASC;
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.metrics
            })
            .then((result)=>{
                for (let i in result) {
                    let date = new Date,
                        monthly = `${result[i].monthly}`,
                        timestamp = new Date(monthly).getTime();

                    output.push({
                            time : timestamp,
                            value : parseFloat(result[i].energy).toFixed(2)
                    })
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
    getenergyusageyearly: (deviceids)=>{
        let output = [],
            query  = ``;

        for (let i in deviceids) {
            if (i == 0) {
                query += `'${deviceids[i]}'`
            } else{
                query += `,'${deviceids[i]}'`

            }
        }
        
        return new Promise((resolve,reject) =>{
            sequelize.query(`
            SELECT * FROM (
                SELECT 
                    yearly, avg(total_usage) AS energy 
                FROM 
                    energy_usage_yearly eud
                WHERE 
                    device_name in (${query})
                GROUP BY 
                    yearly
                ORDER BY 
                    yearly desc 
                LIMIT
                    3
            ) A
            ORDER BY A.yearly ASC;
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.metrics
            })
            .then((result)=>{
                for (let i in result) {
                    let date = new Date,
                        yearly = `${result[i].yearly}`,
                        timestamp = new Date(yearly).getTime();

                    output.push({
                            time : timestamp,
                            value : parseFloat(result[i].energy).toFixed(2)
                    })
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },

    getdeviceenergyusagehourly: (deviceids)=>{
        let output = [],
            query  = ``;
            date   = new Date(),
            hours  = date.getHours();
        for (let i in deviceids) {
            if (i == 0) {
                query += `'${deviceids[i]}'`
            } else{
                query += `,'${deviceids[i]}'`

            }
        }
        
        return new Promise((resolve,reject) =>{
            sequelize.query(`
            SELECT 
                device_name as device, avg("usage") AS energy 
            FROM 
                energy_usage_hourly eud
            WHERE 
                device_name in (${query})
            AND 	
                to_char(hour,'HH24') = '${hours}'
            GROUP BY 
                device
            ORDER BY 
                energy DESC
            LIMIT 10;
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.metrics
            })
            .then((result)=>{
                for (let i in result) {
                    output.push({
                            device : result[i].device,
                            value : parseFloat(result[i].energy).toFixed(2)
                    })
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
    getdeviceenergyusagedaily: (deviceids)=>{
        let output = [],
            query  = ``;
            date   = new Date(),
            hours  = date.getHours();
        for (let i in deviceids) {
            if (i == 0) {
                query += `'${deviceids[i]}'`
            } else{
                query += `,'${deviceids[i]}'`

            }
        }
        
        return new Promise((resolve,reject) =>{
            sequelize.query(`
            SELECT 
                device_name as device, avg("usage") AS energy 
            FROM 
                energy_usage_hourly eud
            WHERE 
                device_name in (${query})
            AND 	
                hour >= date_trunc('day', now())
            GROUP BY 
                device
            ORDER BY 
                energy DESC
            LIMIT 10;
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.metrics
            })
            .then((result)=>{
                for (let i in result) {
                    output.push({
                            device : result[i].device,
                            value : parseFloat(result[i].energy).toFixed(2)
                    })
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
    getdeviceenergyusagemonthly: (deviceids)=>{
        let output = [],
            query  = ``;
            date   = new Date(),
            month_  = date.getMonth() + 1,
            month   = month_ <= 9 ? '0'+month_ : ''+month_;

        for (let i in deviceids) {
            if (i == 0) {
                query += `'${deviceids[i]}'`
            } else{
                query += `,'${deviceids[i]}'`

            }
        }
        
        return new Promise((resolve,reject) =>{
            sequelize.query(`
            SELECT 
                device_name as device, avg(total_usage) AS energy 
            FROM 
                energy_usage_monthly eud2
            WHERE 
                device_name in (${query})
            AND 	
                to_char(monthly,'MM') = '${month}' 
            GROUP BY 
                device
            ORDER BY 
                energy desc
            limit 10;
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.metrics
            })
            .then((result)=>{
                for (let i in result) {
                    output.push({
                            device : result[i].device,
                            value : parseFloat(result[i].energy).toFixed(2)
                    })
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
    getdeviceenergyusageyearly: (deviceids)=>{
        let output = [],
            query  = ``;
            date   = new Date(),
            year   = date.getFullYear();

        for (let i in deviceids) {
            if (i == 0) {
                query += `'${deviceids[i]}'`
            } else{
                query += `,'${deviceids[i]}'`

            }
        }
        
        return new Promise((resolve,reject) =>{
            sequelize.query(`
            SELECT 
                device_name as device, avg(total_usage) AS energy 
            FROM 
                energy_usage_monthly eum
            WHERE 
                device_name in (${query})
            AND 	
                to_char(monthly ,'YYYY') = '${year}' 
            GROUP BY 
                device
            ORDER BY 
                energy desc
            limit 10;
            `, {
                type: QueryTypes.SELECT,
                nest: true,
                mapToModel: true,
                raw: true,
                model: db.metrics
            })
            .then((result)=>{
                for (let i in result) {
                    output.push({
                            device : result[i].device,
                            value : parseFloat(result[i].energy).toFixed(2)
                    })
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },
}